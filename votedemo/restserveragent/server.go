package restserveragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	"gitlab.utc.fr/xuzhenta/ia04_votedemo/votedemo/comsoc"
	rad "gitlab.utc.fr/xuzhenta/ia04_votedemo/votedemo/types"
)

// RestServerAgent pour stocker les bulletins de vote
type RestServerAgent struct {
	sync.Mutex
	id            string
	ballots       map[string]*rad.Ballot
	nombre_ballot int
	addr          string
}

func NewRestServerAgent(addr string) *RestServerAgent {
	New_ballots := make(map[string]*rad.Ballot)
	return &RestServerAgent{id: addr, ballots: New_ballots, addr: addr}
}

// Test de la méthode
func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func (rsa *RestServerAgent) DoNew_Ballot(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()
	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}
	// décodage de la requête comme NewBallot Request
	var req rad.Newballot_Request
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête
	var resp rad.Newballot_Result
	// vérifier le deadline
	deadline, err := time.Parse(time.RFC3339, req.Deadline)
	if err != nil {
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Wrong Time '%s'", req.Deadline)
		w.Write([]byte(msg))
		return
	}
	//vérifier le méthode de vote
	switch req.Rule {
	case "approval", "borda", "condorcet", "copeland", "stv", "majority", "kemeny":
	default:
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Unkonwn rule '%s'", req.Rule)
		w.Write([]byte(msg))
		return
	}
	// vérifier les votants
	if req.Voter_ids == nil {
		w.WriteHeader(http.StatusNotImplemented)
		msg := "No voter ids"
		w.Write([]byte(msg))
		return
	}
	// vérifier le nombre d'alternatives
	if req.Alt < 2 {
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Wrong number of alternatives '%d'", req.Alt)
		w.Write([]byte(msg))
		return
	}
	// créer le VoteID
	New_VoteID := make(map[string]*rad.Vote)
	for _, voter_id := range req.Voter_ids {
		New_VoteID[voter_id] = &rad.Vote{
			AgentID: voter_id,
			Prefs:   nil,
			Options: nil,
		}
	}
	// créer le bulletin de vote
	rsa.nombre_ballot += 1
	ballot := &rad.Ballot{
		ID:       "scrutin" + fmt.Sprint(rsa.nombre_ballot),
		Rule:     req.Rule,
		Deadline: deadline,
		VoterIDs: New_VoteID,
		NumAlts:  req.Alt,
		TieBreak: req.TieBreak,
	}
	rsa.ballots[ballot.ID] = ballot
	resp.Ballot_id = ballot.ID
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}
func (rsa *RestServerAgent) DoVote(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()
	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}
	// décodage de la requête comme Vote Request
	var req rad.Vote_Request
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	// vérifier le ballot_id
	ballot, result := rsa.ballots[req.Vote_id]
	if !result {
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Unknown ballot '%s'", req.Vote_id)
		w.Write([]byte(msg))
		return
	}
	// vérifier le deadline
	if time.Now().After(ballot.Deadline) {
		w.WriteHeader(http.StatusServiceUnavailable)
		msg := fmt.Sprintf("Ballot '%s' is closed", req.Vote_id)
		w.Write([]byte(msg))
		return
	}
	// vérifier le votant
	vote, result := ballot.VoterIDs[req.Agent_id]
	if !result {
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Unknown voter '%s'", req.Agent_id)
		w.Write([]byte(msg))
		return
	}
	if vote.Prefs != nil {
		w.WriteHeader(http.StatusForbidden)
		msg := fmt.Sprintf("Voter '%s' has already voted", req.Agent_id)
		w.Write([]byte(msg))
		return
	}
	// vérifier les préférences
	if len(req.Prefs) != ballot.NumAlts {
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Wrong number of preferences '%d'", len(req.Prefs))
		w.Write([]byte(msg))
		return
	}
	vote.Prefs = make([]int, len(req.Prefs))
	copy(vote.Prefs, req.Prefs)
	vote.Options = make([]int, len(req.Options))
	copy(vote.Options, req.Options)
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal("vote déjà effectué")
	w.Write(serial)
}
func (rsa *RestServerAgent) DoResult(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()
	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}
	// décodage de la requête comme Result Request
	var req rad.Result_Request
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	// vérifier le ballot_id
	ballot, result := rsa.ballots[req.Ballot_id]
	if !result {
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Unknown ballot '%s'", req.Ballot_id)
		w.Write([]byte(msg))
		return
	}
	// vérifier le deadline
	if time.Now().Before(ballot.Deadline) {
		w.WriteHeader(http.StatusUpgradeRequired)
		msg := fmt.Sprintf("Ballot '%s' is not closed", req.Ballot_id)
		w.Write([]byte(msg))
		return
	}
	// envoyer le résultat
	var resp rad.Result_Result
	// vérifier si le vote est complet

	profile := make([][]comsoc.Alternative, 0)
	i := 0
	for _, vote := range ballot.VoterIDs {
		if len(vote.Prefs) == ballot.NumAlts {
			profile = append(profile, make([]comsoc.Alternative, len(vote.Prefs)))
			for j, pref := range vote.Prefs {
				profile[i][j] = comsoc.Alternative(pref)
			}
			i++
		}
	}
	alts := make([]comsoc.Alternative, ballot.NumAlts)
	for i = 1; i <= ballot.NumAlts; i++ {
		alts[i-1] = comsoc.Alternative(i)
	}
	switch ballot.Rule {
	case "majority":
		Winner, err := comsoc.MajoritySCF(profile, alts)
		if err == nil {
			for i := range Winner {
				resp.Winner = append(resp.Winner, int(Winner[i]))
			}
		} else {
			w.WriteHeader(http.StatusNotImplemented)
			msg := fmt.Sprintf("Wrong information in %s", req.Ballot_id)
			w.Write([]byte(msg))
			return
		}
	case "approval":
		Winner, err := comsoc.ApprovalSCF(profile, alts, ballot.TieBreak)
		if err == nil {
			for i := range Winner {
				resp.Winner = append(resp.Winner, int(Winner[i]))
			}
		} else {
			w.WriteHeader(http.StatusNotImplemented)
			msg := fmt.Sprintf("Wrong information in %s", req.Ballot_id)
			w.Write([]byte(msg))
			return
		}
	case "borda":
		Winner, err := comsoc.BordaSCF(profile, alts)
		if err == nil {
			for i := range Winner {
				resp.Winner = append(resp.Winner, int(Winner[i]))
			}
		} else {
			w.WriteHeader(http.StatusNotImplemented)
			msg := fmt.Sprintf("Wrong information in %s", req.Ballot_id)
			w.Write([]byte(msg))
			return
		}
	case "condorcet":
		Winner, err := comsoc.CondorcetWinner(profile, alts)
		if err == nil {
			for i := range Winner {
				resp.Winner = append(resp.Winner, int(Winner[i]))
			}
		} else {
			w.WriteHeader(http.StatusNotImplemented)
			msg := fmt.Sprintf("Wrong information in %s", req.Ballot_id)
			w.Write([]byte(msg))
			return
		}
	case "copeland":
		Winner, err := comsoc.CopelandSCF(profile, alts)
		if err == nil {
			for i := range Winner {
				resp.Winner = append(resp.Winner, int(Winner[i]))
			}
		} else {
			w.WriteHeader(http.StatusNotImplemented)
			msg := fmt.Sprintf("Wrong information in %s", req.Ballot_id)
			w.Write([]byte(msg))
			return
		}
	case "stv":
		Winner, err := comsoc.STV_SCF(profile, alts)
		if err == nil {
			for i := range Winner {
				resp.Winner = append(resp.Winner, int(Winner[i]))
			}
		} else {
			w.WriteHeader(http.StatusNotImplemented)
			msg := fmt.Sprintf("Wrong information in %s", req.Ballot_id)
			w.Write([]byte(msg))
			return
		}
	}
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

// DoCheck pour vérifier les bulletins de vote existants
func (rsa *RestServerAgent) DoCheck(w http.ResponseWriter, r *http.Request) {
	if !rsa.checkMethod("GET", w, r) {
		return
	}

	w.WriteHeader(http.StatusOK)
	rsa.Lock()
	defer rsa.Unlock()
	serial, _ := json.Marshal(rsa.ballots)
	w.Write(serial)

}

func (rsa *RestServerAgent) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/new_ballot", rsa.DoNew_Ballot)
	mux.HandleFunc("/vote", rsa.DoVote)
	mux.HandleFunc("/result", rsa.DoResult)
	mux.HandleFunc("/check", rsa.DoCheck)

	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
