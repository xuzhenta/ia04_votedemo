package types

import (
	"time"
)

// Ballot représente un bulletin de vote créé
type Ballot struct {
	ID        string
	Rule      string
	Deadline  time.Time
	VoterIDs  map[string]*Vote
	NumAlts   int
	TieBreak  []int
	Results   []int
	Completed bool
}

// Vote représente un vote émis
type Vote struct {
	AgentID string
	Prefs   []int
	Options []int
}

// Type for request of /new_ballot
type Newballot_Request struct {
	Rule      string   `json:"rule"`
	Deadline  string   `json:"deadline"`
	Voter_ids []string `json:"voter_ids"`
	Alt       int      `json:"alt"`
	TieBreak  []int    `json:"tie_break"`
}

// Type for request of /vote
type Vote_Request struct {
	Agent_id string `json:"agent_id"`
	Vote_id  string `json:"ballot_id"`
	Prefs    []int  `json:"prefs"`
	Options  []int  `json:"options"`
}

// Type for request of /result
type Result_Request struct {
	Ballot_id string `json:"ballot_id"`
}

type Newballot_Result struct {
	Ballot_id string `json:"ballot_id"`
}

type Result_Result struct {
	Winner  []int `json:"winner"`
	Ranking []int `json:"ranking"`
}
