package main

import (
	"fmt"
	"time"

	ras "gitlab.utc.fr/xuzhenta/ia04_votedemo/votedemo/restvoteragent"
)

func main() {
	agents := []string{"agent1", "agent2", "agent3", "agent4"}
	tie_break := []int{3, 1, 4, 2}
	time_set := time.Now().Add(30 * time.Second)
	ag_Ballot := ras.NewRestClientAgent_Ballot("id1", "http://localhost:8080", "majority", time_set.Format(time.RFC3339), agents, 4, tie_break)
	ag_Ballot.Start()

	ag_vote_1 := ras.NewRestClientAgent_Vote("id2", "http://localhost:8080", "agent1", "scrutin1", []int{1, 2, 3, 4}, []int{1, 2, 3, 4})
	ag_vote_1.Start()

	ag_vote_2 := ras.NewRestClientAgent_Vote("id3", "http://localhost:8080", "agent2", "scrutin1", []int{2, 1, 3, 4}, []int{1, 2, 3, 4})
	ag_vote_2.Start()

	ag_vote_3 := ras.NewRestClientAgent_Vote("id4", "http://localhost:8080", "agent3", "scrutin1", []int{2, 1, 4, 3}, []int{1, 2, 3, 4})
	ag_vote_3.Start()

	time.Sleep(30 * time.Second)
	ag_result := ras.NewRestClientAgent_Result("id5", "http://localhost:8080", "scrutin1")
	ag_result.Start()
	fmt.Scanln()
}
