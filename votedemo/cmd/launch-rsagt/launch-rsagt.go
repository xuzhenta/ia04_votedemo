package main

import (
	"fmt"

	ras "gitlab.utc.fr/xuzhenta/ia04_votedemo/votedemo/restserveragent"
)

func main() {
	server := ras.NewRestServerAgent(":8080")
	server.Start()
	fmt.Scanln()
}
