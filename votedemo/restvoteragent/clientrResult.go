package restvoteragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	rad "gitlab.utc.fr/xuzhenta/ia04_votedemo/votedemo/types"
)

type RestClientAgent_Result struct {
	id        string
	url       string
	ballot_id string
}

func NewRestClientAgent_Result(id string, url string, ballot_id string) *RestClientAgent_Result {
	return &RestClientAgent_Result{id, url, ballot_id}
}

func (rca *RestClientAgent_Result) treatResponse(r *http.Response) (winner []int, ranking []int) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.Result_Result

	json.Unmarshal(buf.Bytes(), &resp)
	winner = resp.Winner
	ranking = resp.Ranking
	return
}

func (rca *RestClientAgent_Result) doRequest() (winner []int, ranking []int, err error) {
	req := rad.Result_Request{
		Ballot_id: rca.ballot_id,
	}

	// sérialisation de la requête
	url := rca.url + "/result"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	winner, ranking = rca.treatResponse(resp)

	return
}

func (rca *RestClientAgent_Result) Start() {
	log.Printf("démarrage de %s", rca.id)
	winner, ranking, err := rca.doRequest()

	if err != nil {
		log.Fatal(rca.id, "error:", err.Error())
	} else {
		log.Printf("Winner is %d\n  ranking is %d\n", winner, ranking)
	}
}
