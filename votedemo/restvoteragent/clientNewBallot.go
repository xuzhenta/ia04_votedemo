package restvoteragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	rad "gitlab.utc.fr/xuzhenta/ia04_votedemo/votedemo/types"
)

type RestClientAgent_Ballot struct {
	id        string
	url       string
	rule      string
	deadline  string
	voter_ids []string
	alt_nb    int
	tie_break []int
}

func NewRestClientAgent_Ballot(id string, url string, rule string, deadline string, voter_ids []string, alt_nb int, tie_break []int) *RestClientAgent_Ballot {
	return &RestClientAgent_Ballot{id, url, rule, deadline, voter_ids, alt_nb, tie_break}
}

func (rca *RestClientAgent_Ballot) treatResponse(r *http.Response) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.Newballot_Result

	json.Unmarshal(buf.Bytes(), &resp)

	return resp.Ballot_id
}

func (rca *RestClientAgent_Ballot) doRequest() (res string, err error) {
	req := rad.Newballot_Request{
		Rule:      rca.rule,
		Deadline:  rca.deadline,
		Voter_ids: rca.voter_ids,
		Alt:       rca.alt_nb,
		TieBreak:  rca.tie_break,
	}

	// sérialisation de la requête
	url := rca.url + "/new_ballot"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	res = rca.treatResponse(resp)

	return
}

func (rca *RestClientAgent_Ballot) Start() {
	log.Printf("démarrage de %s", rca.id)
	ballot_id, err := rca.doRequest()

	if err != nil {
		log.Fatal(rca.id, "error:", err.Error())
	} else {
		log.Printf("%s\n", ballot_id)
	}
}
