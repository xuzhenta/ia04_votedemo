package restvoteragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	rad "gitlab.utc.fr/xuzhenta/ia04_votedemo/votedemo/types"
)

type RestClientAgent_Vote struct {
	id        string
	url       string
	agent_id  string
	ballot_id string
	prefs     []int
	options   []int
}

func NewRestClientAgent_Vote(id string, url string, agent_id string, ballot_id string, prefs []int, options []int) *RestClientAgent_Vote {
	return &RestClientAgent_Vote{id, url, agent_id, ballot_id, prefs, options}
}

func (rca *RestClientAgent_Vote) treatResponse(r *http.Response) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.Newballot_Result

	json.Unmarshal(buf.Bytes(), &resp)

	return resp.Ballot_id
}

func (rca *RestClientAgent_Vote) doRequest() (res string, err error) {
	req := rad.Vote_Request{
		Agent_id: rca.agent_id,
		Vote_id:  rca.ballot_id,
		Prefs:    rca.prefs,
		Options:  rca.options,
	}

	// sérialisation de la requête
	url := rca.url + "/vote"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	res = rca.treatResponse(resp)

	return
}

func (rca *RestClientAgent_Vote) Start() {
	log.Printf("démarrage de %s", rca.id)
	ballot_id, err := rca.doRequest()

	if err != nil {
		log.Fatal(rca.id, "error:", err.Error())
	} else {
		log.Printf("%s", ballot_id)
	}
}
