package comsoc

func BordaSWF(p Profile, alts []Alternative) (count Count, err error) {
	err = checkProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	count = make(Count)
	m := len(p[0])
	for ind := range p {
		for x := range p[ind] {
			count[p[ind][x]] += m - x - 1
		}
	}
	return count, nil

}

func BordaSCF(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p, alts)
	if err != nil {
		return nil, err
	} else {
		return maxCount(count), nil
	}
}
