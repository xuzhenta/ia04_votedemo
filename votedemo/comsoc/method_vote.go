package comsoc

import (
	"errors"
)

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

// renvoie l'indice ou se trouve alt dans prefs
func rank(alt Alternative, prefs []Alternative) int {
	for i, elm := range prefs {
		if alt == elm {
			return i
		}
	}
	return -1
}

// renvoie vrai ssi alt1 est préférée à alt2
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	if rank(alt1, prefs) < rank(alt2, prefs) {
		return true
	}
	return false
}

// renvoie les meilleures alternatives pour un décomtpe donné
func maxCount(count Count) (bestAlts []Alternative) {
	bestAlts = append(bestAlts, 0)
	for k, v := range count {
		if v > count[bestAlts[0]] {
			bestAlts = []Alternative{k}
		} else if v == count[bestAlts[0]] {
			bestAlts = append(bestAlts, k)
		}
	}
	return bestAlts
}

func minCount(count Count) (mauvaisAlts []Alternative) {
	mauvaisAlts = append(mauvaisAlts, 0)
	for k, v := range count {
		if v < count[mauvaisAlts[0]] {
			mauvaisAlts = []Alternative{k}
		} else if v == count[mauvaisAlts[0]] {
			mauvaisAlts = append(mauvaisAlts, k)
		}
	}
	return mauvaisAlts
}

// vérifie les préférences d'un agent, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois
func checkProfile(prefs []Alternative, alts []Alternative) error {
	if len(prefs) != 0 {
		Countmap := make(Count)
		for _, elm := range prefs {
			Countmap[elm]++
		}

		for _, elm := range alts {
			if Countmap[elm] == 0 {
				return errors.New("Les préférences d'un agent ne sont pas complets")
			} else if Countmap[elm] > 1 {
				return errors.New("Une alternative apparaît plus d'une fois dans les préférences")
			}
		}
		return nil
	}
	return nil
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît exactement une fois par préférences
func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	for ind := range prefs {
		err := checkProfile(prefs[ind], alts)
		if err != nil {
			return err
		}
	}
	return nil
}

func TieBreakFactory(orderedAlts []Alternative) func([]Alternative) (Alternative, error) {
	return func(bestAlts []Alternative) (Alternative, error) {
		if bestAlts == nil {
			return -1, errors.New("slice d'alternatives est vide")
		}
		bestrank := rank(bestAlts[0], orderedAlts)
		bestAlt := Alternative(0)
		for _, elm := range bestAlts {
			if rank(elm, orderedAlts) < bestrank {
				bestrank = rank(elm, orderedAlts)
				bestAlt = elm
			}
		}
		return bestAlt, nil
	}
}

func SWFFactory(swfFunc func(p Profile) (Count, error), tiebreakFunc func([]Alternative) (Alternative, error)) func(Profile) ([]Alternative, error) {
	return func(p Profile) ([]Alternative, error) {
		Countmap, err_swf := swfFunc(p)
		if err_swf != nil {
			return nil, err_swf
		}
		alt, err_tie := tiebreakFunc(maxCount(Countmap))
		if err_tie != nil {
			return nil, err_tie
		}
		return []Alternative{alt}, nil
	}
}

func SCFFactory(scfFunc func(p Profile) ([]Alternative, error), tiebreakFunc func([]Alternative) (Alternative, error)) func(Profile) (Alternative, error) {
	return func(p Profile) (Alternative, error) {
		bestalts, err_scf := scfFunc(p)
		if err_scf != nil {
			return -1, err_scf
		}
		bestalt, err_tie := tiebreakFunc(bestalts)
		if err_tie != nil {
			return -1, err_tie
		}
		return bestalt, nil
	}
}
