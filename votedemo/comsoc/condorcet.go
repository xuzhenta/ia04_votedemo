package comsoc

func CondorcetWinner(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {
	err = checkProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	result := true
	for _, elm_1 := range alts {
		result = true
		for _, elm_2 := range alts {
			if elm_2 != elm_1 {
				if !Compare_Condorcet(elm_1, elm_2, p) {
					result = false
					break
				}
			}
		}
		if result == true {
			bestAlts = append(bestAlts, elm_1)
			break
		}
	}
	return bestAlts, nil
}

func Compare_Condorcet(alt1 Alternative, alt2 Alternative, p Profile) bool {
	Vote_1, Vote_2 := 0, 0
	for ind := range p {
		if rank(alt1, p[ind]) < rank(alt2, p[ind]) {
			Vote_1++
		} else {
			Vote_2++
		}
	}
	if Vote_1 > Vote_2 {
		return true
	} else {
		return false
	}
}

func Compare_Copeland(alt1 Alternative, alt2 Alternative, p Profile) int {
	Vote_1, Vote_2 := 0, 0
	for ind := range p {
		if rank(alt1, p[ind]) < rank(alt2, p[ind]) {
			Vote_1++
		} else {
			Vote_2++
		}
	}
	if Vote_1 > Vote_2 {
		return 1
	} else if Vote_1 < Vote_2 {
		return -1
	} else {
		return 0
	}
}

func CopelandSWF(p Profile, alts []Alternative) (count Count, err error) {
	err = checkProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	count = make(Count)
	for _, elm_1 := range alts {
		for _, elm_2 := range alts {
			if elm_2 != elm_1 {
				if Compare_Copeland(elm_1, elm_2, p) == 1 {
					count[elm_1]++
				} else if Compare_Copeland(elm_1, elm_2, p) == -1 {
					count[elm_2]++
				}
			}
		}
	}
	return count, nil
}

func CopelandSCF(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {
	count := make(Count)
	count, err = CopelandSWF(p, alts)
	if err != nil {
		return nil, err
	}
	bestAlts = maxCount(count)
	return bestAlts, nil
}

func Delete_alt(p Profile, alt Alternative) (result Profile) {
	result = make(Profile, 0)
	for _, pref := range p {
		position := rank(alt, pref)
		if position != -1 {
			for j := position; j < len(pref)-1; j++ {
				pref[j] = pref[j+1]
			}
			pref = pref[:len(pref)-1]
			result = append(result, pref)
		}
	}
	return result
}

func Renouvel_profile(p Profile) (result Profile, alt Alternative) {
	count_alt := make(Count)
	for _, pref := range p {
		count_alt[pref[0]]++
	}
	mauvaisAlts := minCount(count_alt)
	alt = mauvaisAlts[0]
	result = Delete_alt(p, alt)
	return result, alt
}

func STV_SWF(p Profile, alts []Alternative) (count Count, err error) {
	err = checkProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	count = make(Count)
	for _, k := range alts {
		count[k] = 1
	}
	result := p
	var alt Alternative
	for i := 1; i < len(alts)-1; i++ {
		result, alt = Renouvel_profile(result)
		count[alt] = -1
	}
	return count, nil

}

func STV_SCF(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {
	count := make(Count)
	count, err = STV_SWF(p, alts)
	if err != nil {
		return nil, err
	}
	bestAlts = maxCount(count)
	return bestAlts, nil
}
