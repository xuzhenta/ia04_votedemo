package comsoc

func ApprovalSWF(p Profile, alts []Alternative, thresholds []int) (count Count, err error) {
	err = checkProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	count = make(Count)
	for ind := range p {
		for x := 0; x < thresholds[ind]; x++ {
			count[p[ind][x]]++
		}
	}
	return count, nil

}
func ApprovalSCF(p Profile, alts []Alternative, thresholds []int) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, alts, thresholds)
	if err != nil {
		return nil, err
	} else {
		return maxCount(count), nil
	}
}
