package comsoc

func MajoritySWF(p Profile, alts []Alternative) (count Count, err error) {

	err = checkProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	count = make(Count)
	for ind := range p {
		count[p[ind][0]]++
	}
	return count, nil
}

func MajoritySCF(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p, alts)
	if err != nil {
		return nil, err
	} else {
		return maxCount(count), nil
	}
}
