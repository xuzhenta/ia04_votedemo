# IA04_Votedemo
Ce package IA04_Votedemo est un package Go conçu pour stocker et gérer les bulletins de vote via une API REST. Ce README fournit un aperçu de ses fonctionnalités et de son utilisation.

Réalisateur : XU Zhentao, Wang Jingbo


## Pour récupérer sur gitlab le package
  
```bash
  set GOPRIVATE=gitlab.utc.fr
```
```bash
  go get gitlab.utc.fr/xuzhenta/ia04_votedemo@v0.2.0
```

```bash
  go install gitlab.utc.fr/xuzhenta/ia04_votedemo/votedemo/cmd/launch-rsagt@v0.2.0 
  go install gitlab.utc.fr/xuzhenta/ia04_votedemo/votedemo/cmd/launch-agent@v0.2.0 
```

## Sommaire

- [IA04\_Votedemo](#ia04_votedemo)
  - [Sommaire](#sommaire)
  - [Usage](#usage)
    - [Étape 1: Démarrer le serveur](#étape-1-démarrer-le-serveur)
    - [Étape 2: Utilisation de différents clients](#étape-2-utilisation-de-différents-clients)
      - [Option 1: Utilisation de `client.go`](#option-1-utilisation-de-clientgo)
        - [Utilisation de `clientNewBallot.go` pour créer un nouveau bulletin](#utilisation-de-clientnewballotgo-pour-créer-un-nouveau-bulletin)
        - [Utilisation de clientVote.go pour soumettre un vote](#utilisation-de-clientvotego-pour-soumettre-un-vote)
        - [Exécutez le client clientResult.go](#exécutez-le-client-clientresultgo)
      - [Option 2: Utilisation de REST Plugin](#option-2-utilisation-de-rest-plugin)
  - [Fonctionnalités implémentées](#fonctionnalités-implémentées)

## Usage

Pour utiliser le code serveur `server.go` avec différents clients pour effectuer des opérations sur le serveur via des URL différentes, vous pouvez suivre les étapes ci-dessous.

### Étape 1: Démarrer le serveur

1. Exécutez le serveur `server.go`. Cela démarrera le serveur REST pour gérer les bulletins de vote. Par défaut, il écoute sur le port `:8080`. Si vous souhaitez utiliser un autre port, modifiez le code en conséquence.

```bash
go run server.go
```

Le serveur est désormais opérationnel et prêt à traiter des demandes.

### Étape 2: Utilisation de différents clients

#### Option 1: Utilisation de `client.go`

##### Utilisation de `clientNewBallot.go` pour créer un nouveau bulletin

Exécutez le client clientNewBallot.go. Il interagira avec le serveur pour créer un nouveau bulletin de vote.

```bash
go run clientNewBallot.go
```

Le client clientNewBallot.go utilisera l'URL par défaut (http://localhost:8080/new_ballot) pour créer un nouveau bulletin de vote et exécutera cette opération sur le serveur.

##### Utilisation de clientVote.go pour soumettre un vote

Exécutez le client clientVote.go. Il interagira avec le serveur pour soumettre un vote.

```bash
go run clientVote.go
```

clientVote.go utilisera l'URL par défaut (http://localhost:8080/vote) pour soumettre un vote et exécutera cette opération sur le serveur.
Utilisation de clientResult.go pour obtenir les résultats

##### Exécutez le client clientResult.go

Il interagira avec le serveur pour obtenir les résultats d'un bulletin de vote.

```bash
go run clientResult.go
```

clientResult.go utilisera l'URL par défaut (http://localhost:8080/result) pour obtenir les résultats d'un bulletin de vote et exécutera cette opération sur le serveur.

En exécutant ces trois clients différents, vous pourrez effectuer diverses opérations sur le serveur, telles que la création de nouveaux bulletins, la soumission de votes et la récupération des résultats. Assurez-vous que chaque client utilise l'URL correcte et les données de demande appropriées pour effectuer l'opération souhaitée. Si nécessaire, vous pouvez modifier l'URL par défaut et les données de demande en fonction des besoins de chaque client.


#### Option 2: Utilisation de REST Plugin

1. Vous pouvez également utiliser des outils de test REST, tels que Postman, Curl, ou des extensions de navigateur comme RESTClient pour interagir avec le serveur en utilisant les URL appropriées.

   - Créez une requête POST pour créer un nouveau bulletin en utilisant l'URL `http://localhost:8080/new_ballot` avec les données appropriées dans le corps de la requête.

   - Soumettez une requête POST pour voter en utilisant l'URL `http://localhost:8080/vote` avec les données de vote appropriées dans le corps de la requête.

   - Envoyez une requête POST pour récupérer les résultats d'un bulletin fermé en utilisant l'URL `http://localhost:8080/result` avec l'ID du bulletin dans le corps de la requête.

   - Effectuez une requête GET pour vérifier les bulletins existants en utilisant l'URL `http://localhost:8080/check`.

Utilisez les URL appropriées et les données de requête pour effectuer des opérations sur le serveur REST, en vous assurant que les données correspondent aux opérations que vous souhaitez effectuer.

Avec ces deux options, vous pouvez utiliser différents clients pour interagir avec le serveur REST en utilisant les URL appropriées pour effectuer des opérations telles que la création de bulletins, la soumission de votes, la récupération des résultats, et la vérification des bulletins existants.


## Fonctionnalités implémentées

1. Créer un Nouveau Bulletin de Vote (DoNew_Ballot):

    Cette fonction permet de créer un nouveau bulletin de vote en spécifiant les règles de vote, la date limite, les identifiants des votants, le nombre d'alternatives, et un critère de départage. Le code vérifie diverses conditions pour s'assurer que les informations du bulletin sont valides, puis crée et stocke le bulletin dans le système.

2. Soumettre un Vote (DoVote):

    La fonction DoVote permet aux électeurs de soumettre leurs votes pour un bulletin spécifique. Elle vérifie que l'électeur n'a pas déjà voté pour le bulletin en question, que le bulletin est ouvert et que les préférences soumises correspondent au nombre d'alternatives. Si ces conditions sont remplies, le vote est enregistré.

3. Récupérer les Résultats (DoResult):

    La fonction DoResult permet de récupérer les résultats d'un bulletin fermé. Elle calcule les résultats en fonction des règles de vote spécifiées, telles que la majorité, l'approbation, le borda, le condorcet, le copeland et le STV. Les résultats sont renvoyés sous forme de réponse.

4. Vérifier les Bulletins Existant (DoCheck):

    La fonction DoCheck permet de vérifier les bulletins existants stockés dans le système. Cette fonction renvoie la liste des bulletins actuellement enregistrés.

Ces fonctions sont appelées en fonction des requêtes HTTP entrantes vers les différents points de terminaison définis dans le serveur, tels que /new_ballot, /vote, /result, et /check. Chaque fonction effectue des vérifications appropriées et répond en conséquence pour assurer l'intégrité du système de gestion des bulletins de vote.


## Explication des variables

### Détail de Ballot
| Nom de param | Type de param | Description de param |
| ------- | ------- | ------- |
| ID   | string   | ID unique d'un ballot   |
| Rule  | string   | Règles de vote   |
| Deadline   | time.Time   | Date limite   |
| VoterIDs  | map[string]*Vote   | Liste de vote des électeurs   |
| NumAlts   | int   | Nombre de candidats   |
| TieBreak  | []int   | L'ordre prédéfini de candidats quand il y a une égalité    |
| Results   | []int   | Candidat final   |
| Completed  | bool   | Déterminer si le vote est complet   |

### Détail de Vote
| Nom de param | Type de param | Description de param |
| ------- | ------- | ------- |
| AgentID   | string   | Nom d'électeur   |
| Prefs     | []int   | Préférences des électeurs   |
| Options   | []int   | Permet de passer des renseignements supplémentaires   |

### Détail de Newballot_Request
| Nom de param | Type de param | Description de param |
| ------- | ------- | ------- |
| Deadline   | string   | Date limite   |
| Voter_ids     | []string  | liste des électeurs   |
| Alt   | int   | Nombre de candidats  |
| TieBreak   | []int   | L'ordre prédéfini de candidats quand il y a une égalité   |

### Détail de Vote_Request
| Nom de param | Type de param | Description de param |
| ------- | ------- | ------- |
| Agent_id   | string   | Nom d'électeur   |
| Vote_id     | string  | Nom de ballot   |
| Prefs   | []int | Préférences des électeurs  |
| Options   | []int   | Permet de passer des renseignements supplémentaires   |

### Détail de Result_Request
| Nom de param | Type de param | Description de param |
| ------- | ------- | ------- |
| Ballot_id   | string   | Nom de ballot  |

### Détail de Newballot_Result
| Nom de param | Type de param | Description de param |
| ------- | ------- | ------- |
| Ballot_id   | string   | Nom de ballot  |

### Détail de Result_Result 
| Nom de param | Type de param | Description de param |
| ------- | ------- | ------- |
| Winner   | []int   | Numéro de candidat gagnant  |
| Ranking   | []int   | Tous les candidats sont triés selon le classement final  |

